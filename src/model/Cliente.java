/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Aluno 02
 */
public class Cliente {

   
    //Atributos
    private int cli_id;
    private String cli_nome;
    private long cli_cpf;
    private String endereco;
    private String cidade;
    private String data_nasc;
    private int cli_usu;
    /**
     * @return the cli_usu
     */
    public int getCli_usu() {
        return cli_usu;
    }

    /**
     * @param cli_usu the cli_usu to set
     */
    public void setCli_usu(int cli_usu) {
        this.cli_usu = cli_usu;
    }

    

    /**
     * @return the cli_id
     */
    public int getCli_id() {
        return cli_id;
    }

    /**
     * @param cli_id the cli_id to set
     */
    public void setCli_id(int cli_id) {
        this.cli_id = cli_id;
    }

    /**
     * @return the cli_nome
     */
    public String getCli_nome() {
        return cli_nome;
    }

    /**
     * @param cli_nome the cli_nome to set
     */
    public void setCli_nome(String cli_nome) {
        this.cli_nome = cli_nome;
    }

    /**
     * @return the cli_cpf
     */
    public long getCli_cpf() {
        return cli_cpf;
    }

    /**
     * @param cli_cpf the cli_cpf to set
     */
    public void setCli_cpf(String txt_cpf) {
        String cpf = txt_cpf;
        Long cli_cpf = Long.valueOf(cpf.replaceAll("\\.", "").replaceAll("\\-", ""));
        this.cli_cpf = cli_cpf;
    }

    /**
     * @return the endereco
     */
    public String getEndereco() {
        return endereco;
    }

    /**
     * @param endereco the endereco to set
     */
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    /**
     * @return the cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @return the data_nasc
     */
    public String getData_nasc() {
        return data_nasc;
    }

    /**
     * @param data_nasc the data_nasc to set
     */
    public void setData_nasc(String data_nasc) {
        this.data_nasc = data_nasc;
    }

}
