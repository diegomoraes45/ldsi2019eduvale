/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Aluno 02
 */
public class LoginDAO {
    
    //Atributos
    Connection con;
    String sql;
    PreparedStatement pst;
    ResultSet rs;
    int codusu;
    
    //Métodos
    //Consultar Usuário e Senha
    public int consultarUsuarioLogin(JTextField loginUsu, JPasswordField senhaUsu,
                                      JFrame jfLogin, JFrame jfPrincipal){
        
        int tipo = 0;
        
        try {
            
            con = Conexao.conectar();
            sql = "select * from usuario where usu_login = ? and usu_senha = ?";
            pst = con.prepareStatement(sql);
            pst.setString(1, loginUsu.getText());
            pst.setString(2, senhaUsu.getText());
            rs = pst.executeQuery();
            
            if(rs.next()){
                
                JOptionPane.showMessageDialog(jfLogin, "Acesso Permitido");
                tipo = rs.getInt("usu_tipo");
                codusu=rs.getInt("usu_id");
                jfPrincipal.setVisible(true);
                jfLogin.setVisible(false);
                
            }else{
                JOptionPane.showMessageDialog(jfLogin, "Acesso Negado");
                loginUsu.setText("");
                senhaUsu.setText("");
                loginUsu.requestFocus();
            }
            
            Conexao.desconectar();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(jfLogin, "Erro ao acessar: "+e); 
        }
        
        return tipo;
    }
    
    public int codUsuario(){
        return codusu;
    }
    
}
