/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.*;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import model.Cliente;
import model.Usuario;
import net.proteanit.sql.DbUtils;
import view.JF_Usuario;

/**
 *
 * @author Aluno 02
 */
public class ClienteDAO {

    //Atributos
    Connection con;
    String sql;
    PreparedStatement pst;
    ResultSet rs;

    //Métodos
    //SALVAR
    public void salvarCliente(Cliente cliente, JFrame jfCliente) {

        try {
            con = Conexao.conectar();
            sql = "insert into Cliente (cli_nome, cli_cpf, cli_endereco, cli_cidade, cli_datanasc, cli_usu)values(?,?,?,?,?,?)";
            pst = con.prepareStatement(sql);
            pst.setString(1, cliente.getCli_nome());
            pst.setLong(2, cliente.getCli_cpf());
            pst.setString(3, cliente.getEndereco());
            pst.setString(4, cliente.getCidade());
            pst.setString(5, cliente.getData_nasc());
            pst.setInt(6, cliente.getCli_usu());
            pst.execute();

            JOptionPane.showMessageDialog(jfCliente, "Cadastrado com Sucesso!");

            Conexao.desconectar();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfCliente, "Erro ao Cadastrar: " + e);
        }

    }

    //ALTERAR
    public void alterarCliente(Cliente cliente, JFrame jfCliente) {
        try {
            con = Conexao.conectar();
            sql = "update Cliente set cli_nome=?, cli_cpf=?, cli_endereco=?, cli_cidade=?, cli_datanasc=?, cli_usu=? where cli_id=?";
            pst = con.prepareStatement(sql);
            pst.setString(1, cliente.getCli_nome());
            pst.setLong(2, cliente.getCli_cpf());
            pst.setString(3, cliente.getEndereco());
            pst.setString(4, cliente.getCidade());
            pst.setString(5, cliente.getData_nasc());
            pst.setInt(6, cliente.getCli_usu());
            pst.setInt(7, cliente.getCli_id());
            pst.execute();

            JOptionPane.showMessageDialog(jfCliente, "Alterado com Sucesso!");

            Conexao.desconectar();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfCliente, "Erro ao Alterar: " + e);
        }
    }

    //APAGAR
    public void apagarCliente(Cliente cliente, JFrame jfCliente) {

        try {
            con = Conexao.conectar();
            sql = "delete from Cliente where cli_id = ?";
            pst = con.prepareStatement(sql);
            pst.setInt(1, cliente.getCli_id());

            if (JOptionPane.showConfirmDialog(jfCliente, "Deseja Deletar?", "Atenção", JOptionPane.YES_NO_CANCEL_OPTION) == 0) {
                pst.execute();
                JOptionPane.showMessageDialog(jfCliente, "Deletado com Sucesso!");
                Conexao.desconectar();
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(jfCliente, "Erro ao deletar: " + e);
        }

    }

    //CONSULTAR POR NOME
    public void consultarClienteNome(JTextField txtPesquisa, JTable tabCliente, JFrame jfCliente) {

        try {
            con = Conexao.conectar();
            sql = "select cli_id as ID, cli_nome as Nome, cli_cpf as CPF from Cliente where cli_nome like ?";
            pst = con.prepareStatement(sql);
            pst.setString(1, txtPesquisa.getText() + "%");
            rs = pst.executeQuery();
            tabCliente.setModel(DbUtils.resultSetToTableModel(rs));
            Conexao.desconectar();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(jfCliente, "Erro ao consultar: " + e);
        }

    }

    //CONSULTAR POR ID
    public void consultarClienteID(int codCliente, JTextField txtid, JTextField txtnome,
            JTextField txtcpf, JTextField txtendereco, JTextField txtcidade,
            JTextField txtdatanasc, JFrame jfCliente) {

        try {
            con = Conexao.conectar();
            sql = "select * from Cliente where cli_id=?";
            pst = con.prepareStatement(sql);
            pst.setInt(1, codCliente);
            rs = pst.executeQuery();

            if (rs.next()) {
                txtid.setText(String.valueOf(rs.getInt("cli_id")));
                txtnome.setText(rs.getString("cli_nome"));
                txtcpf.setText(String.valueOf(rs.getLong("cli_cpf")));
                txtendereco.setText(rs.getString("cli_endereco"));
                txtcidade.setText(rs.getString("cli_cidade"));
                txtdatanasc.setText(rs.getString("cli_datanasc"));
            } else {
                JOptionPane.showMessageDialog(jfCliente, "Nenhum Registro Encontrado");
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(jfCliente, "Erro ao consultar: " + e);
        }

    }
    
    public void consultaView(JTable tbClienteUsuario){
        
        try {
            con = Conexao.conectar();
            sql = "select * from vw_cliente_usuario";
            pst = con.prepareStatement(sql);
            rs=pst.executeQuery();
            
            tbClienteUsuario.setModel(DbUtils.resultSetToTableModel(rs));
            
        } catch (Exception e) {
        }
        
            
        
    }

}
