SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `scc` DEFAULT CHARACTER SET utf8 ;
USE `scc` ;

-- -----------------------------------------------------
-- Table `scc`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `scc`.`usuario` (
  `usu_id` INT(11) NOT NULL AUTO_INCREMENT,
  `usu_login` VARCHAR(45) NOT NULL,
  `usu_senha` VARCHAR(45) NOT NULL,
  `usu_tipo` INT(11) NOT NULL,
  PRIMARY KEY (`usu_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `scc`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `scc`.`cliente` (
  `cli_id` INT(11) NOT NULL AUTO_INCREMENT,
  `cli_nome` VARCHAR(45) NOT NULL,
  `cli_cpf` BIGINT(20) NULL DEFAULT NULL,
  `cli_endereco` VARCHAR(100) NULL DEFAULT NULL,
  `cli_cidade` VARCHAR(45) NOT NULL,
  `cli_datanasc` VARCHAR(10) NULL DEFAULT NULL,
  `cli_usu` INT(11) NOT NULL,
  PRIMARY KEY (`cli_id`),
  UNIQUE INDEX `cli_cpf_UNIQUE` (`cli_cpf` ASC),
  INDEX `cli_usu` (`cli_usu` ASC),
  CONSTRAINT `cliente_ibfk_1`
    FOREIGN KEY (`cli_usu`)
    REFERENCES `scc`.`usuario` (`usu_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

USE `scc` ;

-- -----------------------------------------------------
-- Placeholder table for view `scc`.`vw_cliente_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `scc`.`vw_cliente_usuario` (`NomeCliente` INT, `NomeUsuario` INT);

-- -----------------------------------------------------
-- View `scc`.`vw_cliente_usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `scc`.`vw_cliente_usuario`;
USE `scc`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `scc`.`vw_cliente_usuario` AS select `scc`.`cliente`.`cli_nome` AS `NomeCliente`,`scc`.`usuario`.`usu_login` AS `NomeUsuario` from (`scc`.`cliente` join `scc`.`usuario` on((`scc`.`cliente`.`cli_usu` = `scc`.`usuario`.`usu_id`)));

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
